import { inject } from 'inversify';
import { controller, httpGet } from 'inversify-express-utils';
import { UserService } from '../services/user';
import { IUser } from '../models/user';
import { TYPES } from '../constants/type';

@controller('/user')
export class UserController implements IUserController {
  constructor(@inject(TYPES.UserService) private userService: UserService) {}
  @httpGet('/')
  public getUsers(): IUser[] {
    return this.userService.getUsers();
  }
}

class IUserController {
  // getUsers(): IUser[];
}
