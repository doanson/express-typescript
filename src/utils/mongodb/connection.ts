import mongoose from 'mongoose';
const connStr = 'mongodb://localhost:27017';
export class MongoDBConnection {
  public static async initConnection() {
    await MongoDBConnection.connect();
  }

  public static async connect() {
    return mongoose
      .connect(connStr)
      .then(() => {})
      .catch((err) => {
        console.log('Error connecting to database: ', err);
        return process.exit(1);
      });
  }
}
