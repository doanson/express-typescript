class CommonError extends Error {
  public statusCode: number;
  constructor(message: string, statusCode) {
    super(message);
    this.statusCode = statusCode;
  }
}

class ClientError extends CommonError {}
class InternalServerError extends CommonError {}
export { CommonError, ClientError, InternalServerError };
