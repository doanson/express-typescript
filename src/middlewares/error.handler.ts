import { Response } from 'express';
import * as Err from '../exceptions/index';
import { CommonError } from '../exceptions/index';

function handleError(err: CommonError, res: Response) {
  switch (err.constructor) {
    case Err.InternalServerError:
    //
    case Err.ClientError:
    //
    case Err.CommonError:
      return res.status(err.statusCode).json({
        message: err.message,
        statusCode: err.statusCode
      });
  }
}

export { handleError };
