// import { provide } from "inversify-binding-decorators";
// import { TYPES } from "../constants/type";
import { Schema, model } from 'mongoose';

export interface IUser {
  email: string;
  name: string;
  _id?: string;
  avatar?: string;
  password: string;
}

const userSchema = new Schema<IUser>({
  email: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  password: {
    type: String,
    required: true
  }
});

export const User = model<IUser>('User', userSchema);
