import { injectable } from 'inversify';
import { IUser, User } from '../models/user';

@injectable()
export class UserService implements IUserService {
  public getUsers(): IUser[] {
    return [];
  }
  public async createUser(user: any): Promise<any> {
    return User.create(user)
      .then((user) => {})
      .catch((error: Error) => {
        throw error;
      });
  }
}

interface IUserService {
  getUsers(): IUser[];
  createUser: (user: any) => Promise<any>;
}
