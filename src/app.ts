import 'reflect-metadata';
import { Container } from 'inversify';
import bodyParser from 'body-parser';
import { InversifyExpressServer } from 'inversify-express-utils';
import { UserService } from './services/user';
import { TYPES } from './constants/type';
import './controller/user';
import config from './config';
import { Response, Request, NextFunction } from 'express';
import { handleError } from './middlewares/error.handler';
import { CommonError } from './exceptions';
let container = new Container();

container.bind<UserService>(TYPES.UserService).to(UserService);

let server = new InversifyExpressServer(container);

server.setConfig((app) => {
  app.use(bodyParser.json());
});

server.setErrorConfig((app) => {
  app.use((req: Request, res: Response, next: NextFunction) => {
    return res.status(404).send({ message: 'Not Found', statusCode: 404 });
  });

  app.use(
    (error: CommonError, req: Request, res: Response, next: NextFunction) => {
      return handleError(error, res);
    }
  );
});

server.build().listen(config.app.port, () => {
  console.log('app is running on port 8001');
});
